#include <signal.h>
#include <stdio.h>
#include <unistd.h>
int uid_gen=0;
void handler_alrm(int sig) {
	int uid=uid_gen++;
	printf("%d：收到信号，休眠5秒...\n", uid);
	sleep(5);
	printf("%d：信号函数返回\n", uid);
}
void handler_stop(int sig) {
	printf("stopping...");
}

int main() {
	if (SIG_ERR == signal(SIGALRM, handler_alrm))
	{
		printf("ddd");
	}
//	signal(SIGALRM, handler_alrm);
	signal(SIGSTOP, handler_stop);
	printf("进程%d\n", getpid());
	getchar();
	puts("正常退出");
	return 0;
}
