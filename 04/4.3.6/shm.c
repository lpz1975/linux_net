#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <string.h>

#define KEY_PATH "/home/xxx"

int main()
{
	key_t key;
	key=ftok(KEY_PATH,1);
	printf("key:%#x\n",key);
	if(key < 0)
	{
		perror("ftok");
		exit(-1);
	}
	int shmid;
	if(-1 == (shmid=shmget(key,64,IPC_CREAT | 0600)))//创建
	{
		perror("shmget");
		exit(EXIT_FAILURE);
	}
	
	// puts("创建共享内存成功!");
	printf("key:%d\n",shmid);
	char *padrr=(char *)shmat(shmid,NULL,0);//映射,要进行类型转换
	if(padrr == NULL)
	{
		perror("shmat");
		exit(EXIT_FAILURE);
	}
	strcpy(padrr,"hello");
	shmdt(padrr);//断开
  return 0;
}
